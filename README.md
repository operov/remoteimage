# RemoteImage

[![CI Status](https://img.shields.io/travis/Oleksii Perov/RemoteImage.svg?style=flat)](https://travis-ci.org/Oleksii Perov/RemoteImage)
[![Version](https://img.shields.io/cocoapods/v/RemoteImage.svg?style=flat)](https://cocoapods.org/pods/RemoteImage)
[![License](https://img.shields.io/cocoapods/l/RemoteImage.svg?style=flat)](https://cocoapods.org/pods/RemoteImage)
[![Platform](https://img.shields.io/cocoapods/p/RemoteImage.svg?style=flat)](https://cocoapods.org/pods/RemoteImage)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RemoteImage is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RemoteImage'
```

## Author

Oleksii Perov, operov@griddynamics.com

## License

RemoteImage is available under the MIT license. See the LICENSE file for more info.
